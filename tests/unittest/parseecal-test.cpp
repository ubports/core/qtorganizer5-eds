/*
 * Copyright 2013 Canonical Ltd.
 * Copyright 2022 Guido Berhoerster <guido+qtorganizer5-eds@berhoerster.name>
 *
 * This file is part of qtorganizer5-eds.
 *
 * contact-service-app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * contact-service-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//ugly hack but this allow us to test the engine without mock EDS
#define private public
#include "qorganizer-eds-engine.h"
#undef private

#include "gscopedpointer.h"

#include <QObject>
#include <QtTest>
#include <QDebug>

#include <QtOrganizer>

#include <libecal/libecal.h>

using namespace QtOrganizer;

class ParseEcalTest : public QObject
{
    Q_OBJECT
private:
    static const QString vEvent;
    QList<QOrganizerItem> m_itemsParsed;

public Q_SLOTS:
    void onEventAsyncParsed(QList<QOrganizerItem> items)
    {
        m_itemsParsed = items;
    }

private Q_SLOTS:
    void cleanup()
    {
        m_itemsParsed.clear();
    }

    void testParseStartTime()
    {
        QDateTime startTime = QDateTime::currentDateTime();
        QDateTime endTime(startTime);
        endTime = endTime.addDays(2);

        GScopedPointer<ECalComponent> comp(e_cal_component_new());
        e_cal_component_set_new_vtype(comp.data(), E_CAL_COMPONENT_EVENT);

        ICalTime *itStart = i_cal_time_new_from_timet_with_zone(startTime.toTime_t(), FALSE, NULL);
        ECalComponentDateTime *dtStart = e_cal_component_datetime_new_take(itStart, NULL);
        e_cal_component_set_dtstart(comp.data(), dtStart);
        e_cal_component_datetime_free(dtStart);

        ECalClient *client = nullptr;

        QOrganizerEvent item;
        QOrganizerEDSEngine::parseStartTime(client, comp.data(), &item);
        QCOMPARE(item.startDateTime().toTime_t(), startTime.toTime_t());

        ICalTime *itEnd = i_cal_time_new_from_timet_with_zone(endTime.toTime_t(), FALSE, NULL);
        ECalComponentDateTime *dtEnd = e_cal_component_datetime_new_take(itEnd, NULL);
        e_cal_component_set_dtend(comp.data(), dtEnd);
        e_cal_component_datetime_free(dtEnd);

        QOrganizerEDSEngine::parseEndTime(client, comp.data(), &item);
        QCOMPARE(item.endDateTime().toTime_t(), endTime.toTime_t());
    }

    void testParseRemindersQOrganizerEvent2ECalComponent()
    {
        QOrganizerEvent event;
        QOrganizerItemAudibleReminder aReminder;

        // Check audible reminder
        aReminder.setRepetition(10, 30);
        aReminder.setSecondsBeforeStart(10);
        QCOMPARE(aReminder.secondsBeforeStart(), 10);
        event.saveDetail(&aReminder);

        GScopedPointer<ECalComponent> comp(e_cal_component_new());
        e_cal_component_set_new_vtype(comp.data(), E_CAL_COMPONENT_EVENT);

        QOrganizerEDSEngine::parseReminders(event, comp.data());

        GSList *aIds = e_cal_component_get_alarm_uids(comp.data());
        QCOMPARE(g_slist_length(aIds), (guint) 1);

        ECalComponentAlarm *alarm = e_cal_component_get_alarm(comp.data(), (const gchar*)aIds->data);
        QVERIFY(alarm);

        ECalComponentAlarmAction aAction = e_cal_component_alarm_get_action(alarm);
        QCOMPARE(aAction, E_CAL_COMPONENT_ALARM_AUDIO);

        ECalComponentAlarmTrigger *trigger = e_cal_component_alarm_get_trigger(alarm);
        QCOMPARE(e_cal_component_alarm_trigger_get_kind(trigger), E_CAL_COMPONENT_ALARM_TRIGGER_RELATIVE_START);
        QCOMPARE(i_cal_duration_as_int(e_cal_component_alarm_trigger_get_duration(trigger)) * -1, aReminder.secondsBeforeStart());

        ECalComponentAlarmRepeat *aRepeat = e_cal_component_alarm_get_repeat(alarm);
        QCOMPARE(e_cal_component_alarm_repeat_get_repetitions(aRepeat), aReminder.repetitionCount());
        QCOMPARE(i_cal_duration_as_int(e_cal_component_alarm_repeat_get_interval(aRepeat)), aReminder.repetitionDelay());

         e_cal_component_alarm_free(alarm);
         g_slist_free_full(aIds, g_free);
    }

    void testParseRemindersECalComponent2QOrganizerEvent()
    {
        GScopedPointer<ECalComponent> comp(e_cal_component_new());
        e_cal_component_set_new_vtype(comp.data(), E_CAL_COMPONENT_EVENT);

        ECalComponentAlarm *alarm = e_cal_component_alarm_new();
        e_cal_component_alarm_set_action(alarm, E_CAL_COMPONENT_ALARM_DISPLAY);

        ICalDuration *triggerDuration = i_cal_duration_new_from_int(-10);
        ECalComponentAlarmTrigger *trigger = e_cal_component_alarm_trigger_new_relative(E_CAL_COMPONENT_ALARM_TRIGGER_RELATIVE_START, triggerDuration);
        g_object_unref(triggerDuration);
        e_cal_component_alarm_take_trigger(alarm, trigger);

        ICalDuration *alarmDuration = i_cal_duration_new_from_int(100);
        ECalComponentAlarmRepeat *aRepeat = e_cal_component_alarm_repeat_new(5, alarmDuration);
        g_object_unref(alarmDuration);
        e_cal_component_alarm_take_repeat(alarm, aRepeat);

        e_cal_component_add_alarm(comp.data(), alarm);
        e_cal_component_alarm_free(alarm);

        QOrganizerItem alarmItem;
        QOrganizerEDSEngine::parseReminders(comp.data(), &alarmItem);

        QOrganizerItemVisualReminder vReminder = alarmItem.detail(QOrganizerItemDetail::TypeVisualReminder);
        QCOMPARE(vReminder.repetitionCount(), 5);
        QCOMPARE(vReminder.repetitionDelay(), 100);
        QCOMPARE(vReminder.secondsBeforeStart(), 10);
    }

    void testParseRecurenceQOrganizerEvent2ECalComponent()
    {
        // by date
        QOrganizerEvent event;
        QOrganizerItemRecurrence rec;

        QList<QDate> rDates;
        rDates << QDate(2010, 1, 20)
               << QDate(2011, 2, 21)
               << QDate(2012, 3, 22);

        rec.setRecurrenceDates(rDates.toSet());

        QList<QDate> rExeptDates;
        rExeptDates << QDate(2013, 4, 23)
                    << QDate(2014, 5, 24)
                    << QDate(2015, 6, 25);
        rec.setExceptionDates(rExeptDates.toSet());

        QOrganizerRecurrenceRule dailyRule;
        QList<QOrganizerRecurrenceRule> rrules;

        dailyRule.setFrequency(QOrganizerRecurrenceRule::Daily);
        dailyRule.setLimit(1000);
        rrules << dailyRule;

        QOrganizerRecurrenceRule weeklyRule;
        weeklyRule.setFrequency(QOrganizerRecurrenceRule::Weekly);
        weeklyRule.setLimit(1001);
        QList<Qt::DayOfWeek> daysOfWeek;
        daysOfWeek << Qt::Monday
                   << Qt::Tuesday
                   << Qt::Wednesday
                   << Qt::Thursday
                   << Qt::Friday;
        weeklyRule.setDaysOfWeek(daysOfWeek.toSet());
        weeklyRule.setFirstDayOfWeek(Qt::Sunday);
        rrules << weeklyRule;

        QOrganizerRecurrenceRule monthlyRule;
        monthlyRule.setFrequency(QOrganizerRecurrenceRule::Monthly);
        monthlyRule.setLimit(1002);

        QList<int> daysOfMonth;
        daysOfMonth << 1
                    << 15
                    << 30;
        monthlyRule.setDaysOfMonth(daysOfMonth.toSet());
        rrules << monthlyRule;

        QOrganizerRecurrenceRule yearlyRule;
        yearlyRule.setFrequency(QOrganizerRecurrenceRule::Yearly);
        yearlyRule.setLimit(1003);
        QList<int> daysOfYear;
        daysOfYear << 1
                    << 10
                    << 20
                    << 50
                    << 300;
        yearlyRule.setDaysOfYear(daysOfYear.toSet());


        QList<QOrganizerRecurrenceRule::Month> monthsOfYear;
        monthsOfYear << QOrganizerRecurrenceRule::January
                     << QOrganizerRecurrenceRule::March
                     << QOrganizerRecurrenceRule::December;
        yearlyRule.setMonthsOfYear(monthsOfYear.toSet());
        rrules << yearlyRule;

        rec.setRecurrenceRules(rrules.toSet());

        // save recurrence
        event.saveDetail(&rec);

        ECalComponent *comp = e_cal_component_new();
        e_cal_component_set_new_vtype(comp, E_CAL_COMPONENT_EVENT);
        QOrganizerEDSEngine::parseRecurrence(event, comp);

        // recurrence dates
        GSList *periodList = e_cal_component_get_rdates(comp);

        QCOMPARE(g_slist_length(periodList), (guint)3);

        for(GSList *pIter = periodList; pIter != 0; pIter = pIter->next) {
            ECalComponentPeriod *period = static_cast<ECalComponentPeriod*>(pIter->data);
            QDate periodDate = QDateTime::fromTime_t(i_cal_time_as_timet(e_cal_component_period_get_start(period))).date();

            QVERIFY(rDates.contains(periodDate));
        }
        g_slist_free_full(periodList, e_cal_component_period_free);

        // exception dates
        GSList *exDateList = e_cal_component_get_exdates(comp);
        for(GSList *pIter = exDateList; pIter != 0; pIter = pIter->next) {
            ECalComponentDateTime *exDate = static_cast<ECalComponentDateTime*>(pIter->data);
            QDate exDateValue = QDateTime::fromTime_t(i_cal_time_as_timet(e_cal_component_datetime_get_value(exDate))).date();
            QVERIFY(rExeptDates.contains(exDateValue));
        }
        g_slist_free_full(exDateList, e_cal_component_datetime_free);


        // rules
        GSList *recurList = e_cal_component_get_rrules(comp);
        QCOMPARE(g_slist_length(recurList), (guint) rrules.count());

        for(GSList *recurListIter = recurList; recurListIter != 0; recurListIter = recurListIter->next) {
            ICalRecurrence *rule = static_cast<ICalRecurrence*>(recurListIter->data);

            switch(i_cal_recurrence_get_freq(rule))
            {
            case I_CAL_DAILY_RECURRENCE:
                QCOMPARE(i_cal_recurrence_get_count(rule), dailyRule.limitCount());
                break;
            case I_CAL_WEEKLY_RECURRENCE:
                QCOMPARE(i_cal_recurrence_get_count(rule), weeklyRule.limitCount());
                for (int d = Qt::Monday; d <= Qt::Sunday; d++) {
                    if (daysOfWeek.contains(static_cast<Qt::DayOfWeek>(d))) {
                        QVERIFY(i_cal_recurrence_get_by_day(rule, (d-1)) != I_CAL_RECURRENCE_ARRAY_MAX);
                    } else {
                        QVERIFY(i_cal_recurrence_get_by_day(rule, d-1) == I_CAL_RECURRENCE_ARRAY_MAX);
                    }
                }
                break;
            case I_CAL_MONTHLY_RECURRENCE:
            {
                QCOMPARE(i_cal_recurrence_get_count(rule), monthlyRule.limitCount());

                QList<int> ruleDays;
                for (int d=0; d < I_CAL_BY_MONTHDAY_SIZE; d++) {
                    if (i_cal_recurrence_get_by_month_day(rule, d) != I_CAL_RECURRENCE_ARRAY_MAX) {
                        ruleDays << i_cal_recurrence_get_by_month_day(rule, d);
                    }
                }
                QCOMPARE(ruleDays.count(), daysOfMonth.count());
                Q_FOREACH(int day, ruleDays) {
                    QVERIFY(daysOfMonth.contains(day));
                }
                break;
            }
            case I_CAL_YEARLY_RECURRENCE:
            {
                QCOMPARE(i_cal_recurrence_get_count(rule), yearlyRule.limitCount());
                QList<int> ruleDays;

                for (int d=0; d < I_CAL_BY_YEARDAY_SIZE; d++) {
                    if (i_cal_recurrence_get_by_year_day(rule, d) != I_CAL_RECURRENCE_ARRAY_MAX) {

                        ruleDays << i_cal_recurrence_get_by_year_day(rule, d);
                    }
                }
                QCOMPARE(ruleDays.count(), daysOfYear.count());
                Q_FOREACH(int day, ruleDays) {
                    QVERIFY(daysOfYear.contains(day));
                }

                QList<int> ruleMonths;
                for (int d=0; d < I_CAL_BY_MONTH_SIZE; d++) {
                    if (i_cal_recurrence_get_by_month(rule, d) != I_CAL_RECURRENCE_ARRAY_MAX) {
                        ruleMonths << i_cal_recurrence_get_by_month(rule, d);
                    }
                }
                QCOMPARE(ruleMonths.count(), monthsOfYear.count());
                Q_FOREACH(int month, ruleMonths) {
                    QVERIFY(monthsOfYear.contains(static_cast<QOrganizerRecurrenceRule::Month>(month)));
                }
            }
            default:
                break;
            }
        }

        g_slist_free_full(recurList, g_object_unref);

        ECalClient *client = nullptr;
        // invert
        QOrganizerEvent event2;
        QOrganizerEDSEngine::parseRecurrence(client, comp, &event2);

        QCOMPARE(event2.recurrenceDates(), event.recurrenceDates());
        QCOMPARE(event2.exceptionDates(), event.exceptionDates());

        QList<QOrganizerRecurrenceRule> rrules2 = event2.recurrenceRules().toList();
        QCOMPARE(rrules2.count(), rrules.count());

        Q_FOREACH(const QOrganizerRecurrenceRule &rule2, rrules2) {
            switch(rule2.frequency()) {
            case QOrganizerRecurrenceRule::Daily:
                QCOMPARE(rule2.limitCount(), dailyRule.limitCount());
                break;
            case QOrganizerRecurrenceRule::Weekly:
            {
                QCOMPARE(rule2.limitCount(), weeklyRule.limitCount());
                QList<Qt::DayOfWeek> daysOfWeek2 = rule2.daysOfWeek().toList();
                qSort(daysOfWeek2);
                QCOMPARE(daysOfWeek2, daysOfWeek);
                break;
            }
            case QOrganizerRecurrenceRule::Monthly:
            {
                QCOMPARE(rule2.limitCount(), monthlyRule.limitCount());
                QList<int> daysOfMonth2 = rule2.daysOfMonth().toList();
                qSort(daysOfMonth2);
                QCOMPARE(daysOfMonth2, daysOfMonth);
                break;
            }
            case QOrganizerRecurrenceRule::Yearly:
            {
                QCOMPARE(rule2.limitCount(), yearlyRule.limitCount());
                QList<int> daysOfYear2 = rule2.daysOfYear().toList();
                qSort(daysOfYear2);
                QCOMPARE(daysOfYear2, daysOfYear);

                QList<QOrganizerRecurrenceRule::Month> monthsOfYear2 = rule2.monthsOfYear().toList();
                qSort(monthsOfYear2);
                QCOMPARE(monthsOfYear2, monthsOfYear);
                break;
            }
            default:
                QVERIFY(false);
            }
        }

        g_object_unref(comp);
    }

    void testAsyncParse()
    {
        qRegisterMetaType<QList<QOrganizerItem> >();
        QOrganizerEDSEngine *engine = QOrganizerEDSEngine::createEDSEngine(QMap<QString, QString>());
        QVERIFY(engine);
        ICalComponent *ical = i_cal_component_new_from_string(vEvent.toUtf8().data());
        QVERIFY(ical);
        QVERIFY(i_cal_component_is_valid(ical));

        QList<QOrganizerItemDetail::DetailType> detailsHint;
        GSList *events = g_slist_append(NULL, ical);
        QMap<QByteArray, GSList*> eventMap;
        eventMap.insert(engine->defaultCollectionId().localId(), events);
        engine->parseEventsAsync(eventMap, true, detailsHint, this, SLOT(onEventAsyncParsed(QList<QOrganizerItem>)));

        QTRY_COMPARE(m_itemsParsed.size(), 1);
        QOrganizerEvent ev = m_itemsParsed.at(0);
        QDateTime eventTime(QDate(2015,04, 8), QTime(19, 0, 0), QTimeZone("America/Recife"));
        QCOMPARE(ev.startDateTime(), eventTime);
        QCOMPARE(ev.endDateTime(), eventTime.addSecs(30 * 60));
        QCOMPARE(ev.displayLabel(), QStringLiteral("one minute after start"));
        QCOMPARE(ev.description(), QStringLiteral("event to parse"));

        QOrganizerRecurrenceRule rrule = ev.recurrenceRule();
        QCOMPARE(rrule.frequency(), QOrganizerRecurrenceRule::Daily);
        QCOMPARE(rrule.limitType(), QOrganizerRecurrenceRule::NoLimit);

        QList<QDate> except = ev.exceptionDates().toList();
        qSort(except);
        QCOMPARE(except.size(), 2);
        QCOMPARE(except.at(0), QDate(2015, 4, 9));
        QCOMPARE(except.at(1), QDate(2015, 5, 1));

        QOrganizerItemVisualReminder vreminder = ev.detail(QOrganizerItemDetail::TypeVisualReminder);
        QCOMPARE(vreminder.secondsBeforeStart(), 60);
        QCOMPARE(vreminder.message(), QStringLiteral("alarm to parse"));

        g_slist_free_full(events, (GDestroyNotify)g_object_unref);
        delete engine;
    }
};



const QString ParseEcalTest::vEvent = QStringLiteral(""
"BEGIN:VEVENT\r\n"
"UID:20150408T215243Z-19265-1000-5926-24@renato-ubuntu\r\n"
"DTSTAMP:20150408T214536Z\r\n"
"DTSTART;TZID=/freeassociation.sourceforge.net/Tzfile/America/Recife:\r\n"
" 20150408T190000\r\n"
"DTEND;TZID=/freeassociation.sourceforge.net/Tzfile/America/Recife:\r\n"
" 20150408T193000\r\n"
"TRANSP:OPAQUE\r\n"
"SEQUENCE:6\r\n"
"SUMMARY:one minute after start\r\n"
"DESCRIPTION:event to parse\r\n"
"CLASS:PUBLIC\r\n"
"CREATED:20150408T215308Z\r\n"
"LAST-MODIFIED:20150409T200807Z\r\n"
"RRULE:FREQ=DAILY\r\n"
"EXDATE;VALUE=DATE:20150501\r\n"
"EXDATE;VALUE=DATE:20150409\r\n"
"BEGIN:VALARM\r\n"
"X-EVOLUTION-ALARM-UID:20150408T215549Z-19265-1000-5926-38@renato-ubuntu\r\n"
"ACTION:DISPLAY\r\n"
"TRIGGER;VALUE=DURATION;RELATED=START:-PT1M\r\n"
"DESCRIPTION:alarm to parse\r\n"
"END:VALARM\r\n"
"END:VEVENT\r\n"
"");


QTEST_MAIN(ParseEcalTest)

#include "parseecal-test.moc"
